﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SalesVid.Backend.Startup))]
namespace SalesVid.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
