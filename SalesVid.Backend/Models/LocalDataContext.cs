﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesVid.Backend.Models
{
    public class LocalDataContext: Domain.Models.DataContext
    {
        public System.Data.Entity.DbSet<SalesVid.Common.Models.Products> Products { get; set; }
    }
}